import React from 'react';
import banner from 'assets/img/architect.jpg';
import map from 'assets/img/map.jpg';
import useFetch from 'hooks/useFetch';
import CategoryCard from './_components/CategoryCard';

const App = () => {

  const { data: dataCategories, loading, error } = useFetch("https://api-tdp-2022.vercel.app/api/categories")

  if (error) console.log(error)

  return (
    <>
      <header className="display-container content wide" style={{ maxWidth: '1500px' }} id="home">
        <img className="image" src={banner} alt="Architecture" width={1500} height={800} />
        <div className="display-middle center">
          <h1 className="xxlarge text-white">
            <span className="padding black opacity-min">
              <b>EDTS</b>
            </span>
            <span className="hide-small text-light-grey">Mart</span>
          </h1>
        </div>
      </header>
      <div className="content padding" style={{ maxWidth: '1564px' }}>
        <div className="container padding-32" id="projects">
          <h3 className="border-bottom border-light-grey padding-16">Products Category</h3>
        </div>
        <div className="row-padding">
          {loading
            ? <div className="row-padding padding-large" style={{ fontSize: '16px', fontWeight: '800', textAlign: 'center' }}>
              Loading . . .
            </div>
            : dataCategories?.data.map(ctg => <CategoryCard key={ctg?.id} item={ctg} />)}
        </div>
        <div className="container padding-32">
          <img src={map} className="image" alt="maps" style={{ width: '100%' }} />
        </div>
      </div>
    </>
  );
};

export default App;
