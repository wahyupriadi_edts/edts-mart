import React from 'react';
import { Link } from 'react-router-dom';

const CategoryCard = ({ item: data }) => {
  return (
    <div className="col l3 m6 margin-bottom">
      <Link to={`/shop?category=${data?.id}`}>
        <div
          className="display-container"
          style={{ boxShadow: '0 2px 7px #dfdfdf', display: 'flex', alignItems: 'center', justifyContent: 'center', height: '300px', padding: '80px' }}
        >
          <div className="display-topleft black padding">{data?.name}</div>
          <img src={data?.image} alt="House" style={{ maxWidth: '100%', minHeight: '100%' }} />
        </div>
      </Link>
    </div>
  );
};

export default CategoryCard;
