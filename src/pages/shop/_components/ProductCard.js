import React from 'react';
import { Link } from 'react-router-dom';

const ProductCard = ({ item: data }) => {
  return (
    <div className="product-card">
      {data?.discount > 0 && <div className="badge">Discount</div>}
      <div className="product-tumb">
        <img src={data?.image} alt="product1" />
      </div>
      <div className="product-details">
        <span className="product-catagory">{data?.category}</span>
        <h4>
          <Link to={`/shop/${data?.id}`}>{data?.title}</Link>
        </h4>
        <p>{data?.description}</p>
        <div className="product-bottom-details">
          <div className="product-price">IDR {new Intl.NumberFormat('id-ID').format(data?.price ?? 0)}</div>
          <div className="product-links">
            <Link to={`/shop/${data?.id}`}>
              View Detail<i className="fa fa-heart"></i>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductCard;
