import useFetch from 'hooks/useFetch';
import React from 'react';
import { useSearchParams } from 'react-router-dom';
import ProductCard from './_components/ProductCard';

const App = () => {
  const [searchParams] = useSearchParams();
  const { data: dataProducts, loading, error } = useFetch(
    'https://api-tdp-2022.vercel.app/api/products',
    { category: searchParams.get('category') }
  );

  // if (loading)
  //   return (
  //     <div className="content padding" style={{ maxWidth: '1564px' }}>
  //       <div className="container" style={{ marginTop: '80px' }}>
  //         <h1>Loading...</h1>
  //       </div>
  //     </div>
  //   );
  // if (error) console.log(error);
  return (
    <div className="content padding" style={{ maxWidth: '1564px' }}>
      <div className="container" style={{ marginTop: '80px' }}>
        <div className="breadcrumb">
          <span>
            <a href="/">Home</a>
            <span className="breadcrumb-separator">/</span>
          </span>
          <span>
            <a href="/">Shop</a>
          </span>
        </div>
      </div>
      <div className="container padding-32" id="about">
        <h3 className="border-bottom border-light-grey padding-16">All Product</h3>
      </div>
      <div className="row-padding rm-before-after" style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'center' }}>
        {loading
          ? <div className="row-padding padding-large" style={{ fontSize: '16px', fontWeight: '800', textAlign: 'center' }}>
            Loading . . .
          </div>
          : dataProducts?.data?.productList.map(prd => <ProductCard key={prd.id} item={prd} />)}
      </div>
    </div>
  );
};

export default App;
