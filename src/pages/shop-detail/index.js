import useFetch from 'hooks/useFetch';
import React, { useMemo, useState } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';

const App = () => {
  const { id } = useParams();
  const navigate = useNavigate();
  const { data: productData, loading, error } = useFetch(`https://api-tdp-2022.vercel.app/api/products/${id}`);
  const { data } = { ...productData };

  const [quantity, setQuantity] = useState(0);
  const subTotal = useMemo(() => {
    const base = (data?.price ?? 0) * quantity;
    const sale = base - base * (data?.discount / 100);
    return { base, sale };
  }, [data, quantity]);

  const handleAddToCart = () => {
    alert('Product added to cart successfully');
    navigate('/shop');
  };

  const handleQuantity = (qty) => {
    if (Number(qty) < 0 || Number(qty) > data?.stock) return
    return setQuantity(Number(qty))
  }

  if (error) console.log(error);
  return (
    <div className="content padding" style={{ maxWidth: '1564px' }}>
      <div className="container" style={{ marginTop: '80px' }}>
        <div className="breadcrumb">
          <span>
            <Link to="/">Home</Link>
            <span className="breadcrumb-separator">/</span>
          </span>
          <span>
            <Link to="/shop">Shop</Link>
            <span className="breadcrumb-separator">/</span>
            <span>Detail</span>
          </span>
        </div>
      </div>
      <div className="container padding-32" id="about">
        <h3 className="border-bottom border-light-grey padding-16">Product Information</h3>
      </div>
      {loading
        ? <div className="row-padding padding-large" style={{ fontSize: '16px', fontWeight: '800', textAlign: 'center' }}>
          Loading . . .
        </div>
        : (
          <div className="row-padding card card-shadow padding-large" style={{ marginBottom: '24px' }}>
            <div className="col l3 m6 margin-bottom">
              <div className="product-tumb">
                <img src={data?.image} alt="Product 1" />
              </div>
            </div>
            <div className="col m6 margin-bottom">
              <h3>{data?.title}</h3>
              <div style={{ marginBottom: '32px' }}>
                <span>
                  Category : <strong>{data?.category}</strong>
                </span>
                <span style={{ marginLeft: '30px' }}>
                  Review : <strong>{data?.rate}</strong>
                </span>
                <span style={{ marginLeft: '30px' }}>
                  Stock : <strong>{data?.stock}</strong>
                </span>
                <span style={{ marginLeft: '30px' }}>
                  Discount : <strong>{data?.discount}%</strong>
                </span>
              </div>
              <div style={{ fontSize: '2rem', lineHeight: '34px', fontWeight: '800', marginBottom: '32px' }}>IDR {new Intl.NumberFormat('id-ID').format(data?.price ?? 0)}</div>
              <div style={{ marginBottom: '32px' }}>{data?.description}</div>
              <div style={{ marginBottom: '32px' }}>
                <div>
                  <strong>Quantity : </strong>
                </div>
                <input
                  type="number"
                  className="input section border"
                  name="total"
                  placeholder="Quantity"
                  value={quantity}
                  onChange={(e) => handleQuantity(e.target.value)}
                  min={0}
                  max={data?.stock}
                />
              </div>
              <div style={{ marginBottom: '32px', fontSize: '2rem', fontWeight: 800 }}>
                Sub Total : IDR {new Intl.NumberFormat('id-ID').format(subTotal.sale ?? 0)}
                {data?.discount > 0 && quantity > 0 && (
                  <span style={{ marginLeft: '30px', fontSize: '18px', textDecoration: 'line-through' }}>
                    <strong>IDR {new Intl.NumberFormat('id-ID').format(subTotal.base ?? 0)}</strong>
                  </span>
                )}
              </div>
              {data?.stock === 0 ? (
                <button className="button light-grey block" disabled={true}>
                  {' '}
                  Empty Stock
                </button>
              ) : (
                quantity > 0 && (
                  <button className="button light-grey block" onClick={handleAddToCart}>
                    Add to cart
                  </button>
                )
              )}
            </div>
          </div>
        )}

    </div>
  );
};

export default App;
