import 'assets/css/style.css';
import 'assets/css/custom.css';
import { default as RoutesList } from 'configs/Route';
import { Route, Routes } from 'react-router-dom';

const App = () => {
  return (
    <>
      <div className="top" >
        <div className="bar white wide padding card">
          <a href="/" className="bar-item button"><b>EDTS</b> TDP Batch #2</a>
          <div className="right hide-small">
            <a href="/" className="bar-item button">Home</a>
            <a href="/shop" className="bar-item button">Shop</a>
          </div>
        </div>
      </div>
      <Routes>
        {RoutesList.map(route => (
          <Route
            key={route.path}
            index={route.path === '/'}
            path={route.path}
            element={route.component}
          />
        ))}
      </Routes>
      <footer className="center black padding-16">
        <p>Copyright 2022</p>
      </footer>
    </>
  );
}

export default App;
