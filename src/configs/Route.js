import Home from 'pages/home'
import Shop from 'pages/shop'
import ShopDetail from 'pages/shop-detail'

const Routes = [
  { path: '/', name: 'Home', component: <Home /> },
  { path: '/shop', name: 'Shop', component: <Shop /> },
  { path: '/shop/:id', name: 'Shop Detail', component: <ShopDetail /> },
]

export default Routes